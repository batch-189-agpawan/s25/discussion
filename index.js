// // alert('hi')

// /*
// 	JSON objects
// 		JSON stands for JavaScript Object Notation
// 		JSON is also used in other progamming languages hence the name JavaScript Object Notation.

// 		Syntax:
// 			{
// 				"propertyA": "valueA"
// 				"propertyB": "valueB"
// 			}

// */

// // JSON Object
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// // // JSON array
// 	"cities" = [
// 		{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
// 		{"city": "Manila", "province": "Metro Manila", "country": "Philippines"}
// 		{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
// 	]


// 	// JSON Methods
// 		// The JSON object contains methods for parsing and converting data into a stringified JSON

// Converting Data into a stringified JSON

let batchesArr = [
	{ batchName: "Batch 189"},
	{ batchName: "Batch 190"}
]

console.log(batchesArr);

// The stringify method is used to convert JS objects into a string
// Before sending data, convert an array or object to its string equivalent
	// stringify
console.log(`Result from a stringified method:`)
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data)

// User Detail
// let firstName = prompt("What is your first name?")
// let lastName = prompt("What is your last name?")
// let age = prompt("What is your age?")
// let address = {
// 	city: prompt("Which city do you live in?"),
// 	country: prompt("Which country does your city address belong to?"),
// }


// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(otherData);

// Convert stringified JSON into JS objects
	// JSON.parse()

	let batchesJSON = `[
	{
		"batchName": "Batch 189"
	},
	{
		"batchName": "Batch 190"
	}]`

console.log(batchesJSON)

// Upong receiving data, the JSON text can be converted to JS object so that we can use it in our program
console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `{
	"name": "Ivy",
	"age": "18",
	"address": {
		"city": "Caloocan City",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject)
console.log(JSON.parse(stringifiedObject))